import { Component, OnInit } from '@angular/core';

export interface NewsArticle {
  title: string;
  content: string;
}

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.css']
})
export class NewsComponent implements OnInit {
  articles = newsMock;

  constructor() { }

  ngOnInit() {
  }

}

const newsMock: NewsArticle[] = [
  {
    title: 'Mon premier article',
    content: 'Article d\'introduction',
  },
  {
    title: 'La V1 est sortie !',
    content: 'Lis toutes les informations à propos de la V1 !',
  },
  {
    title: 'Article long',
    content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras id luctus metus, tincidunt \
    suscipit orci. Donec vel auctor metus, eu ullamcorper mi. Sed ut arcu tincidunt, fermentum risus \
    in, eleifend sapien. Praesent dignissim, lacus convallis consectetur condimentum, sapien nibh \
    placerat mauris, eget auctor nibh felis ut odio. Suspendisse aliquet viverra est, ac gravida \
    mauris pulvinar sed. Sed sit amet facilisis nisl. Duis aliquet, nisi sed iaculis cursus, odio \
    velit rhoncus lectus, quis bibendum velit nisl iaculis purus. Aenean efficitur sem eget interdum \
    elementum. Ut ut accumsan elit. Sed eget felis at nisi vestibulum sollicitudin vitae vitae turpis. \
    Aliquam porta congue nibh, nec pharetra quam maximus et.',
  }
];
