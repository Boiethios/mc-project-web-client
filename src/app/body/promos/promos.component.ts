import { Component, OnInit } from '@angular/core';

export interface PromosArticle {
  content: string;
}

@Component({
  selector: 'app-promos',
  templateUrl: './promos.component.html',
  styleUrls: ['./promos.component.css']
})
export class PromosComponent implements OnInit {
  articles = promosMock;

  constructor() { }

  ngOnInit() {
  }

}

const promosMock: PromosArticle[] = [
  {
    content: 'Pack premium pour la V1',
  },
];
